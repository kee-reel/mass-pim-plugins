Personal Information Management plugins compatible with https://gitlab.com/c4rb0n_un1t/MASS-Plugins

Plugins description:
- GridMainMenuView - UI plugin that provides application main menu with grid layout. Each icon in this menu represents loaded UI plugin;
- HabitsTracker - habits tracking logic
- HabitsTrackerDataExtention - habits tracking data
- HabitsTrackerView - habits tracking UI plugin
- PluginLinkerView - UI plugin that shows loaded plugins, allows to unload loaded plugins and load new plugins
- PomodoroManager - pomodoro technique logic
- PomodoroManagerView - pomodoro technique UI plugin
- PomodoroSettingsDataExtention - pomodoro technique settings data
- TaskSketchManager - simple draw tool logic and data
- TaskSketchManagerView - simple draw tool UI plugin
- ToDoListView - tasks UI plugin (on QML)
- UserTaskDataExtention - tasks data (isDone, name)
- UserTaskDateDataExtention - tasks data (date)
- UserTaskManagerView - tasks UI plugin (on QWidgets)
- UserTaskNotes - tasks notes and data (text)
- UserTaskNotesView - tasks notes UI plugin
- UserTaskPomodoroDataExtention - pomodoro technique data for tasks (pomodorosFinished)
- UserTaskProjectDataExtention - tasks project data (isDone)
- UserTaskRepeatDataExtention - repeated tasks data (repeatType, isSpawnedNewTask)
- UserTasksCalendar - calendar logic
- UserTasksCalendarView - calendar UI plugin
