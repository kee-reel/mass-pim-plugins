#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IUserTaskDateDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskDateDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskDateDataExtention, IUserTaskDataExtention, {"startDate", "dueDate", "timeCost"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	QDateTime startDate() override
	{
		return QDateTime::fromMSecsSinceEpoch(0, Qt::TimeSpec::UTC);
	}

	QDateTime dueDate() override
	{
		return QDateTime::fromMSecsSinceEpoch(0, Qt::TimeSpec::UTC);
	}

	QDateTime timeCost() override
	{
		return QDateTime::fromMSecsSinceEpoch(0, Qt::TimeSpec::UTC);
	}

	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
