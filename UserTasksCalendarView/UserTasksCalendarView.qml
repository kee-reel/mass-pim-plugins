import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    property var taskCalendarObject: taskCalendar !== undefined ? taskCalendar.object : null
	property var ratio: 1

    Rectangle {
        anchors.fill: parent
        color: "#3b4252"
    }
	
	Button {
		id: exitButton
		width: 60 * ratio
		height: 60 * ratio
		topPadding: 2
		icon.source: "qrc:/res/back.png"
		icon.color: "#eceff4"
		icon.width: 30 * ratio
		icon.height: 30 * ratio
		onClicked: uiElement.closeSelf()
		anchors.left: parent.left
		anchors.top: parent.top
		anchors.leftMargin: 10
		anchors.topMargin: 20
		anchors.bottomMargin: 55
		background: Rectangle {
			color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
			radius: 90
		}
	}

    CalendarItem {
		id: calendarComponent
		anchors.top: exitButton.bottom
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.bottom: parent.bottom
    }
	
	function update()
	{
		calendarComponent.updateCalendar()
	}
    
}
