import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.12

Button {
	id: menuItem
	property var uid: -1
	property var name: ""
	Layout.preferredWidth: 100 * ratio
	Layout.preferredHeight: Layout.preferredWidth
	Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
	onClicked: uiElement.openLink(uid)
	background: Rectangle {
		id: backgroundRect
		color: menuItem.pressed ? "#88c0d0" : "#81a1c1"
		radius: 10
	}
	Text {
		text: name[0] + name[1]
		anchors.fill: parent
		anchors.topMargin: 15 * ratio
		color: "#eceff4"
		font.pixelSize: 68 * ratio
		verticalAlignment: Text.AlignVCenter
		horizontalAlignment: Text.AlignHCenter
	}
	Text {
		id: itemName
		text: name
		font.pixelSize: 18 * ratio
		anchors.topMargin: 10 * ratio
		anchors.top: menuItem.bottom
		anchors.left: menuItem.left
		anchors.right: menuItem.right
		horizontalAlignment: Text.AlignHCenter
		color: "#eceff4"
	}
}
