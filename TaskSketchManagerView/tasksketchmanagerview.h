#pragma once


#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/UIElementBase/uielementbase.h"

#include "../../Interfaces/Utility/i_user_task_image_data_ext.h"

#include <QGraphicsScene>

namespace Ui
{
class Form;
}

//! \addtogroup TaskSketchManager_dep
//!  \{
class TaskSketchManagerView : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "TimeKeeper.Module.Test" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	TaskSketchManagerView();
	virtual ~TaskSketchManagerView() = default;

	// PluginBase interface
private:
	void onReady() override;

private slots:
	void buttonClose_clicked();
	void onTreeViewClicked(const QModelIndex& index);
	void addImage();
	void deleteImage();

private:
	QSharedPointer<Ui::Form> ui;
	QPointer<UIElementBase> m_elementBase;

	ReferenceInstancePtr<IUserTaskImageDataExtention> myModel;
	QPointer<IExtendableDataModelFilter> m_filterUserTasks;

	QModelIndex currentIndex;
	QGraphicsScene *scene;
};
//!  \}

