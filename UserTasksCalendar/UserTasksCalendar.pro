
TARGET = UserTasksCalendar
TEMPLATE = lib
QT += core

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)


HEADERS += \
	plugin.h \
	usertaskscalendar.h

SOURCES += \
	plugin.cpp \
	usertaskscalendar.cpp

DISTFILES += PluginMeta.json

