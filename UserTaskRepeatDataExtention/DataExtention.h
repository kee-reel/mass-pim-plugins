#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IUserTaskRepeatDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskRepeatDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskRepeatDataExtention, IUserTaskDataExtention, {"repeatType", "isSpawnedNewTask"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
		connect(this, &DataExtention::modelChanged, this, &DataExtention::onModelSet);
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;
	void spawnedNewTask(int completedTaskId, int spawnedTaskId) override;

public:
	int repeatType() override
	{
		return RepeatType::NONE;
	}

	bool isSpawnedNewTask() override
	{
		return false;
	}

	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

private slots:
	void onModelSet()
	{
		connect(m_model.data(), SIGNAL(itemUpdated(int, const ExtendableItemDataMap&, const ExtendableItemDataMap&)),
		        this, SLOT(onTaskCompleted(int, const ExtendableItemDataMap&, const ExtendableItemDataMap&)));
	}

	void onTaskCompleted(int itemId, const ExtendableItemDataMap& before, const ExtendableItemDataMap& after)
	{
		auto repeatData = after[INTERFACE(IUserTaskRepeatDataExtention)];
		int repeatType = repeatData["repeatType"].toInt();
		if(repeatType == RepeatType::NONE)
			return;

		int isSpawnedNewTask = repeatData["isSpawnedNewTask"].toBool();
		if(isSpawnedNewTask)
			return;

		bool newIsDoneValue = after[INTERFACE(IUserTaskDataExtention)]["isDone"].toBool();
		if(newIsDoneValue && before[INTERFACE(IUserTaskDataExtention)]["isDone"].toBool() != newIsDoneValue)
		{
			auto data = m_model->getItem(itemId);
			auto index = m_model->getItemIndex(itemId);

			auto newStartDate = data[INTERFACE(IUserTaskDateDataExtention)]["startDate"].toDateTime();
			auto newDueDate = data[INTERFACE(IUserTaskDateDataExtention)]["dueDate"].toDateTime();

			switch (repeatType)
			{
			case RepeatType::DAILY:
				newStartDate = newStartDate.addDays(1);
				newDueDate = newDueDate.addDays(1);
				break;
			case RepeatType::WEEKLY:
				newStartDate = newStartDate.addDays(7);
				newDueDate = newDueDate.addDays(7);
				break;
			case RepeatType::MONTHLY:
				newStartDate = newStartDate.addMonths(1);
				newDueDate = newDueDate.addMonths(1);
				break;
			case RepeatType::YEARLY:
				newStartDate = newStartDate.addYears(1);
				newDueDate = newDueDate.addYears(1);
				break;
			}

			data[INTERFACE(IUserTaskRepeatDataExtention)]["isSpawnedNewTask"] = true;
			m_model->updateItem(itemId, data);

			data[INTERFACE(IUserTaskDataExtention)]["isDone"] = false;
			data[INTERFACE(IUserTaskRepeatDataExtention)]["isSpawnedNewTask"] = false;
			data[INTERFACE(IUserTaskDateDataExtention)]["startDate"] = newStartDate;
			data[INTERFACE(IUserTaskDateDataExtention)]["dueDate"] = newDueDate;
			int spawnedTaksId = m_model->addItem(data, index.row(), index.parent());
			emit spawnedNewTask(itemId, spawnedTaksId);
		}
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
