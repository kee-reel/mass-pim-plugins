import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

Item {
    id: root
    property var habitsTrackerObject: habitsTracker !== undefined ? habitsTracker.object : null
    property var ratio: 1

    SystemPalette {
        id: systemPalette
    }

    Rectangle {
        anchors.fill: parent
        color: "#3b4252"
    }

    Button {
        id: exitButton
        width: 60 * ratio
        height: width
        topPadding: 2 * ratio
        icon.source: "qrc:/res/back.png"
        icon.color: "#eceff4"
        icon.width: width / 2
        icon.height: icon.width
        onClicked: uiElement.closeSelf()
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 10 * ratio
        anchors.topMargin: 20 * ratio
        anchors.bottomMargin: 55 * ratio
        background: Rectangle {
            color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
            radius: 90
        }
    }

    ColumnLayout {
        anchors.top: exitButton.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 15 * ratio
        spacing: 15 * ratio

        Label {
            Layout.fillWidth: true

            text: "Total habbit streaks: " + habitsTrackerObject.totalStreak
            color: "#d8dee9"
            font.pixelSize: 58 * ratio
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: "New habbit name:"
                color: "#d8dee9"
            }

            Rectangle {
                color: systemPalette.window
                Layout.fillWidth: true
                height: addHabbitButton.height
                TextEdit {
                    id: habbitName
                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                    color: systemPalette.text
                    font.pixelSize: 18 * ratio
                }
            }

            Button {
                id: addHabbitButton
                text: "Start"
                onClicked: {
                    habitsTrackerObject.addHabbit(habbitName.text)
                    habbitName.text = ""
                }
            }
        }

        HabitsList {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
