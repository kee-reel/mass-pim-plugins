
TARGET = HabitsTracker
TEMPLATE = lib
QT += core

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)


HEADERS += \
	../Interfaces/Utility/i_habits_tracker.h \
	../Interfaces/Utility/i_habits_tracker_data_ext.h \
	habitstracker.h \
	plugin.h

SOURCES += \
	habitstracker.cpp \
	plugin.cpp

DISTFILES += PluginMeta.json

