#pragma once


#include <QApplication>
#include <QDebug>
#include <QLayout>
#include <QAbstractProxyModel>
#include <QPainter>
#include <QAbstractItemModel>
#include <QResizeEvent>


#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/UIElementBase/uielementbase.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_pomodoro_data_ext.h"

#include "designproxymodel.h"
#include "addform.h"
#include "mytreeview.h"
#include "checkboxitemdelegate.h"

namespace Ui
{
class Form;
}

//! \addtogroup UserTaskManager_dep
//!  \{
class UserTaskManagerView : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "TimeKeeper.Module.Test" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	UserTaskManagerView();
	virtual ~UserTaskManagerView() = default;

	// PluginBase interface
private:
	virtual void onReady() override;

private slots:
	void OnAddFormClosed();
	void onTreeViewClicked(const QModelIndex &index);
	void buttonExit_clicked();

private:
	QSharedPointer<Ui::Form> ui;
	QPointer<UIElementBase> m_uiElementBase;

	ReferenceInstancePtr<IUserTaskDataExtention> m_taskManager;
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_dateTaskManager;
	ReferenceInstancePtr<IUserTaskRepeatDataExtention> m_repeatTaskManager;
	ReferenceInstancePtr<IUserTaskPomodoroDataExtention> m_pomodoroTaskManager;
	QIdentityProxyModel *proxyModel;
	QPointer<IExtendableDataModel> taskTree;
	QPointer<IExtendableDataModelFilter> taskTreeFilter;

	DesignProxyModel *model;
	bool expandFlag;
	QModelIndex currentModelIndex;
	CheckBoxItemDelegate* m_checkBoxItemDelegate;
};
//!  \}

