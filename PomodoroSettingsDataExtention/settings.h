#pragma once

#include <QtCore>

#include "../../Interfaces/Utility/ipomodorosettingsdataextention.h"

#include "pomodorosettingsdataextention.h"

class Settings : public QObject, public IPomodoroSettingsData
{
	Q_OBJECT
	Q_INTERFACES(IPomodoroSettingsData)
	Q_PROPERTY(quint16 workSessionDuration READ workSessionDuration WRITE setWorkSessionDuration)
	Q_PROPERTY(quint16 easyRestDuration READ easyRestDuration WRITE setEasyRestDuration)
	Q_PROPERTY(quint16 longRestDuration READ longRestDuration WRITE setLongRestDuration)
	Q_PROPERTY(quint16 longRestPeriod READ longRestPeriod WRITE setLongRestPeriod)

public:
	Settings(QObject* parent, DataExtention* dataExtention) :
		QObject(parent),
		m_dataExtention(dataExtention)
	{
	}
	virtual ~Settings() = default;

	void init()
	{
		auto model = m_dataExtention->getModel();
		if(model->rowCount() == 0)
		{
			QMap<Interface, QVariantMap>&& itemData = {
				{	INTERFACE(IPomodoroSettingsData), {
						{"workSessionDuration", m_dataExtention->workSessionDuration()},
						{"easyRestDuration", m_dataExtention->easyRestDuration()},
						{"longRestDuration", m_dataExtention->longRestDuration()},
						{"longRestPeriod", m_dataExtention->longRestPeriod()}
					}
				}
			};
			model->appendItem(itemData);
		}
	}

	// IPomodoroSettingsData interface
private:
	quint16 workSessionDuration() override
	{
		return getData("workSessionDuration");
	}
	void setWorkSessionDuration(quint16 value) override
	{
		setData("workSessionDuration", value);
	}
	quint16 easyRestDuration() override
	{
		return getData("easyRestDuration");
	}
	void setEasyRestDuration(quint16 value) override
	{
		setData("easyRestDuration", value);
	}
	quint16 longRestDuration() override
	{
		return getData("longRestDuration");
	}
	void setLongRestDuration(quint16 value) override
	{
		setData("longRestDuration", value);
	}
	quint16 longRestPeriod() override
	{
		return getData("longRestPeriod");
	}
	void setLongRestPeriod(quint16 value) override
	{
		setData("longRestPeriod", value);
	}

private:
	quint16 getData(const char* attrName)
	{
		auto model = m_dataExtention->getModel();
		auto index = model->index(0, 0);
		auto data = model->getItem(index);
		return data[INTERFACE(IPomodoroSettingsData)][attrName].toUInt();
	}

	void setData(const char* attrName, quint16 value)
	{
		auto model = m_dataExtention->getModel();
		auto index = model->index(0, 0);
		auto data = model->getItem(index);
		data[INTERFACE(IPomodoroSettingsData)][attrName] = value;
		model->updateItem(index, data);
	}

private:
	DataExtention *m_dataExtention;
};
