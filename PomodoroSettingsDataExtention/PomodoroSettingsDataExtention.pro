
TARGET = PomodoroSettingsDataExtention
TEMPLATE = lib
QT += core

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)


HEADERS += \
	plugin.h \
	pomodorosettingsdataextention.h \
	settings.h

SOURCES += \
	plugin.cpp

DISTFILES += PluginMeta.json

