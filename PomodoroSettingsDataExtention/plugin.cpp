#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_dataExtention(new DataExtention(this)),
	m_settings(new Settings(this, m_dataExtention))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IPomodoroSettingsData), m_settings},
		{INTERFACE(IDataExtention), m_dataExtention},
	}
	);
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_settings->init();
}
