#include "plugin.h"

PomodoroManager::PomodoroManager() :
	PluginBase(this),
	m_dataExtention(new DataExtention(this, m_userTask))
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IDataExtention), m_dataExtention},
		{INTERFACE(IUserTaskPomodoroDataExtention), m_dataExtention}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_userTask}
	});
}

PomodoroManager::~PomodoroManager()
{
}
