#-------------------------------------------------
#
# Project created by QtCreator 2017-02-01T20:08:55
#
#-------------------------------------------------
TARGET = UserTaskNotes
TEMPLATE = lib
QT += widgets

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

SOURCES += \
    tasksketchmanager.cpp

HEADERS +=\
    tasksketchmanager.h \
    DataExtention.h \
    ../../Interfaces/Utility/i_user_task_notes_data_ext.h

DISTFILES += \
    PluginMeta.json
